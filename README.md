# Vagrant virtual host environment #

This is a Vagrant configuration for multi-site LAMP (Linux, Apache, MySQL and PHP) development. Virtual Host configuration allows developers to define domain directories like www.demo.vg and access them with http://www.demo.vg:8080/ URL.

# Requirements #

* Minimum Vagrant version: **1.6**

# Features #

This vagrant instance comes with these pre-installed packages:

* **apache2**
* **curl**
* **php7**
* **mysql** (with admin password: *root*)

These external ports are opened:

* **3306**: MySQL Server
* **8080**: Web Server (apache2)

Apache virtual hosts configuration allows to serve multiple domains. To create a new domain, just make a directory with domain name in vhosts directory (vhosts/www.yourdomain.com).

# Domain provisioning #

This vagrant instance allows domain provisioning, to install/configure new packages or initialize databases. Provisioning script will be executed once.

To create your provisioning script for your domain:

* Make a new directory *bootstrap* in your domain directory (vhosts/*www.yourdomain.com*/**bootstrap**/)
* Copy in this directory your provisioning files (SQL initialization script for example)
* Create a *setup.sh* file (vhosts/*www.yourdomain.com*/bootstrap/**setup.sh**)
* Edit *setup.sh* with your initialization commands (commands are relative to *bootstrap* directory):

```
#!bash

#!/bin/sh
mysql < yourdomain.sql --password=root
```

* Make *setup.sh* executable

```
#!bash

chmod +x vhosts/www.yourdomain.com/bootstrap/setup.sh
```

# Demo #

Folder vhosts/www.demo.vg is a demo project, with an active domain provisioning script (vhosts/www.demo.vg/bootstrap/setup.sh). To test it, first edit your HOSTS file (path for Mac OS X and Linux: /etc/hosts) adding this entry:

127.0.0.1 www.demo.vg

then start vagrant and after the boot point your browser to:

http://www.demo.vg:8080/

# Enhancements #

Vagrant cannot listen on port 80 on Mac. If you want to forward port 80 connections to the vagrant port 8080, please take a look to this guide: http://simonelaicorner.blogspot.it/2014/11/vagrant-on-port-80-mac-os-x-yosemite.html