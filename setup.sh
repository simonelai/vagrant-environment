#!/usr/bin/env bash

apt update

#installare apache2
apt install -y apache2

#installare mysql con password "root"
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt install -y mysql-server

#installare curl
apt install -y curl

#installare sendmail (per la funzione mail())
apt install -y sendmail-bin sendmail

#installare php
apt install -y php libapache2-mod-php php7.0 php7.0-common php7.0-gd php7.0-mysql php7.0-imap php7.0-cli php7.0-cgi php-pear php7.0-mcrypt imagemagick php7.0-curl php7.0-intl php7.0-pspell php7.0-recode php7.0-sqlite3 php7.0-tidy php7.0-xmlrpc php7.0-xsl php-memcache php-imagick php-gettext php7.0-zip php7.0-mbstring php7.0-soap php7.0-fpm php7.0-opcache php-apcu php-bcmath
phpenmod gd
phpenmod mcrypt

a2dismod mpm_event
a2enmod mpm_prefork
a2enmod php7.0

#abilito mod_rewrite di apache2 per: permalinks
a2enmod rewrite

#abilito vhost_alias di apache2 per: virtual hosting dinamico
a2enmod vhost_alias
echo "UseCanonicalName Off" 									>	/etc/apache2/mods-enabled/vhost_alias.conf
echo "VirtualDocumentRoot /var/www/vhosts/%0" 					>> 	/etc/apache2/mods-enabled/vhost_alias.conf
echo "ScriptAlias /cgi-bin/ /var/www/cgi-bin/" 					>> 	/etc/apache2/mods-enabled/vhost_alias.conf
systemctl restart apache2

#cambiare impostazioni apache per leggere .htaccess
cat /etc/apache2/apache2.conf | awk '/<Directory \/var\/www\/>/,/AllowOverride None/{sub("None","All",$0)}{print}' > /etc/apache2/apache2-new.conf
mv /etc/apache2/apache2.conf /etc/apache2/apache2-old.conf
mv /etc/apache2/apache2-new.conf /etc/apache2/apache2.conf
systemctl restart apache2

#cambiare impostazioni mysql per rispondere a tutti "127.0.0.1" => "0.0.0.0"
sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mariadb.conf.d/50-server.cnf
systemctl restart mysql

locale-gen it_IT.UTF-8
locale-gen it_IT
