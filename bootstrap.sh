#!/usr/bin/env bash

#eseguire gli script dentro la cartella bootstrap/
for dir in /var/www/vhosts/*; 
do
	dirname=${dir##*/}
	if [[ -d "$dir" ]]; 
	then
		if [[ -d "$dir/bootstrap" ]];
		then
			if [[ ! -f "$dir/bootstrap/done" ]]; 
			then
				echo "$dirname: Provisioning...";
				(cd "$dir/bootstrap" && ./setup.sh && touch "./done");
			else
				echo "$dirname: Skipped (already provisioned).";
			fi
			if [[ -f "$dir/bootstrap/onstart.sh" ]];
			then
				echo "$dirname: Starting script...";
				(cd "$dir/bootstrap" && ./onstart.sh);
			fi
		else
			echo "$dirname: Skipped (no bootstrap directory found).";
		fi
	fi
done
